#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh

setup_lodevice

export DISK="${LDEVICE}"
export LVM_PART_NAME="lvm-test"

prepare_disk

OUTPUT=$(fdisk -l "${DISK}")

assert_contain "${OUTPUT}" "Disklabel type: gpt" "Disklabel is not gpt"
assert_contain "${OUTPUT}" "2G Linux LVM" "LVM partition is not 2G"

# vi: sts=4 ts=4 sw=4 ai
