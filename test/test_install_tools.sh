#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh


function cleanup() {
    set +e
    pkill -9 dirmngr
    umount /mnt/gains/dev/pts
    umount /mnt/gains/dev/mqueue
    umount /mnt/gains/dev/shm
    umount /mnt/gains/dev
    umount /mnt/gains/sys/kernel/debug
    umount /mnt/gains/sys/kernel/config
    umount /mnt/gains/sys/kernel/security
    umount /mnt/gains/sys/firmware/efi/efivars
    umount /mnt/gains/sys/fs/cgroup
    umount /mnt/gains/sys/fs/pstore
    umount /mnt/gains/sys
    umount /mnt/gains/boot/efi
    umount /mnt/gains/boot
    umount /mnt/gains/dev

    umount /mnt/gains/proc
    swapoff /dev/lvmtestvg/swap
    umount /dev/mapper/lvmtestvg-root
    # deactivate
    vgchange -a n "${LVM_VG_NAME}"
    vgremove -y "${LVM_VG_NAME}"
    # close the encrypted partition
    cryptsetup close "${CRYPT_NAME}"
    test -d /mnt/gains/boot/efi && rmdir -v /mnt/gains/boot/efi
    test -d /mnt/gains/boot/ && rmdir -v /mnt/gains/boot
    test -d /mnt/gains/ && rmdir -v /mnt/gains

}


cleanall() {
    cleanup
    cleanup_lodevice
}

trap cleanall EXIT

setup_lodevice

export DISK="${LDEVICE}"
export LVM_PART_NAME="lvmtest"
export LVM_VG_NAME="${LVM_PART_NAME}vg"
export DISKPASSWORD="verYSekretPassword!"
export CRYPT_NAME="crypt_${LVM_PART_NAME}"
#export STAGE3_URL="https://ftp.fau.de/gentoo/releases/amd64/autobuilds/current-stage3-amd64-openrc-splitusr/stage3-amd64-openrc-splitusr-20240428T163427Z.tar.xz"
export STAGE3_TARBALL="stage3-amd64-openrc-splitusr-20240505T170430Z.tar.xz"
export SWAPSIZE="256M"
export NCPUS="8"
export FETCH_USER="oznt"
export SHORTCUT_RSYNC="y"
export FETCH_PASSWORD="${FETCH_PASSWORD:-s3kr3t}"
export BINPKGS_MIRROR="https://gains.shift-computing.de"
export GLOBAL_USE="vala lvm"
export VIDEO_CARDS="intel"

prepare_disk
encrypt_partition
create_lvm
format_partitions
mount_partitions "${DISK}"
get_and_unpack_base_system
prepare_chroot
add_bin_repo
add_make_conf
set_lvm_use_flags
prepare_install
install_tools

emerge_logs=/mnt/gains/var/log/emerge.log

read -p "Press enter to continue"
cp -v /mnt/gains/var/log/emerge.log ./emerge.log

assert_file_contains ${emerge_logs} "Merging Binary (sys-fs/lvm2" "No lvm2"
assert_file_contains ${emerge_logs} "Merging Binary (sys-fs/cryptsetup" "No cryptsetup"
assert_file_contains ${emerge_logs} "Merging Binary (app-admin/logrotate" "No logrotate"
assert_file_contains ${emerge_logs} "Merging Binary (sys-apps/plocate" "No plocate"
assert_file_contains ${emerge_logs} "Merging Binary (sys-process/fcron" "No fcron"
assert_file_contains ${emerge_logs} "Merging Binary (app-admin/sysklogd" "No sysklogd"
assert_file_contains ${emerge_logs} "Merging Binary (sys-boot/grub-2" "No grub-2"
assert_file_contains ${emerge_logs} "Merging Binary (sys-firmware/intel-microcode" "No intel-microcode"
assert_file_contains ${emerge_logs} "Merging Binary (sys-kernel/linux-firmware" "No linux-firmware"
assert_file_contains ${emerge_logs} "Merging Binary (net-misc/networkmanager" "No networkmanager"
assert_file_contains ${emerge_logs} "Merging Binary (net-misc/openssh" "No openssh"
assert_file_contains ${emerge_logs} "Merging Binary (app-admin/doas" "No doas"
assert_file_contains ${emerge_logs} "Merging Binary (sys-kernel/dracut" "No dracut"
assert_file_contains ${emerge_logs} "Merging Binary (sys-kernel/installkernel" "No dracut"
assert_file_contains ${emerge_logs} "Merging Binary (sys-kernel/gentoo-kernel-bin" "No gentoo-kernel-bin"

assert_file_exists /mnt/gains/sbin/lvm "No lvm"
#assert_file_exists /mnt/gains/sbin/cryptsetup "No cryptsetup"
#assert_file_exists /mnt/gains/etc/cron.daily/logrotate "No logrotate"
#assert_file_exists /usr/share/man/man8/fcron.8.bz2 "No fcron manpage"

read -p "Press enter to continue"
echo "All tests passed!"
