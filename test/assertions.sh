# https://github.com/torokmark/assert.sh/blob/main/assert.sh#L164

if command -v tput &>/dev/null && tty -s; then
  RED=$(tput setaf 1)
  GREEN=$(tput setaf 2)
  MAGENTA=$(tput setaf 5)
  NORMAL=$(tput sgr0)
  BOLD=$(tput bold)
else
  RED=$(echo -en "\e[31m")
  GREEN=$(echo -en "\e[32m")
  MAGENTA=$(echo -en "\e[35m")
  NORMAL=$(echo -en "\e[00m")
  BOLD=$(echo -en "\e[01m")
fi

log_header() {
  printf "\n${BOLD}${MAGENTA}==========  %s  ==========${NORMAL}\n" "$@" >&2
}

log_success() {
  printf "${GREEN}✔ %s${NORMAL}\n" "$@" >&2
}

log_failure() {
  printf "${RED}✖ %s${NORMAL}\n" "$@" >&2
}

assert_contain() {
  local haystack="$1"
  local needle="${2-}"
  local msg="${3-}"

  if [ -z "${needle:+x}" ]; then
    return 0;
  fi

  if [ -z "${haystack##*$needle*}" ]; then
    return 0
  else
    [ "${#msg}" -gt 0 ] && log_failure "$haystack doesn't contain $needle :: $msg" || true
    return 1
  fi
}

assert_file_contains() {
  local file="$1"
  local needle="$2"
  local msg="${3-}"
  if [ -z "${needle:+x}" ]; then
    return 0;
  fi

  if grep -q "$needle" "$file" ; then
    return 0
  else
    [ "${#msg}" -gt 0 ] && log_failure "$file doesn't contain $needle :: $msg" || true
    return 1
  fi
}

assert_eq() {
  local expected="$1"
  local actual="$2"
  local msg="${3-}"

  if [ "$expected" == "$actual" ]; then
    return 0
  else
    [ "${#msg}" -gt 0 ] && log_failure "$expected == $actual :: $msg" || true
    return 1
  fi
}

assert_equal () {
  assert_eq "$@"
}

assert_file_exists() {
  local file="$1"
  local msg="${2-}"

  if [ -f "$file" ]; then
    return 0
  else
    [ "${#msg}" -gt 0 ] && log_failure "$file does not exist :: $msg" || true
    return 1
  fi
}

assert_link_exists() {
  local file="$1"
  local msg="${2-}"

  if [ -L "$file" ]; then
    return 0
  else
    [ "${#msg}" -gt 0 ] && log_failure "$file does not exist :: $msg" || true
    return 1
  fi
}
