#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh

setup_lodevice

export DISK="${LDEVICE}"
export LVM_PART_NAME="lvm-test"
export DISKPASSWORD="verYSekretPassword!"

prepare_disk
assert_eq "$(calculate_third_partition_name)" "${DISK}p3"

encrypt_partition

cryptsetup isLuks "${DISK}p3" || (echo "Partition is not encrypted" && exit 1)

export CRYPT_NAME="crypt_${LVM_PART_NAME}"
cryptsetup close "${CRYPT_NAME}"

trap cleanup_lodevice EXIT
# vi: sts=4 ts=4 sw=4 ai
