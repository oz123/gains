#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh


function cleanup() {
    set +e
    pkill -9 dirmngr
    umount /mnt/gains/dev/pts
    umount /mnt/gains/dev/mqueue
    umount /mnt/gains/dev/shm
    umount /mnt/gains/dev
    umount /mnt/gains/sys/kernel/debug
    umount /mnt/gains/sys/kernel/config
    umount /mnt/gains/sys/kernel/security
    umount /mnt/gains/sys/firmware/efi/efivars
    umount /mnt/gains/sys/fs/cgroup
    umount /mnt/gains/sys/fs/pstore
    umount /mnt/gains/sys
    umount /mnt/gains/boot/efi
    umount /mnt/gains/boot
    umount /mnt/gains/dev

    umount /mnt/gains/proc
    swapoff /dev/lvmtestvg/swap
    umount /dev/mapper/lvmtestvg-root
    # deactivate
    vgchange -a n "${LVM_VG_NAME}"
    vgremove -y "${LVM_VG_NAME}"
    # close the encrypted partition
    cryptsetup close "${CRYPT_NAME}"
    test -d /mnt/gains/boot/efi && rmdir -v /mnt/gains/boot/efi
    test -d /mnt/gains/boot/ && rmdir -v /mnt/gains/boot
    test -d /mnt/gains/ && rmdir -v /mnt/gains

}


cleanall() {
    cleanup
    cleanup_lodevice
}

trap cleanall EXIT

setup_lodevice

export DISK="${LDEVICE}"
export LVM_PART_NAME="lvmtest"
export LVM_VG_NAME="${LVM_PART_NAME}vg"
export DISKPASSWORD="verYSekretPassword!"
export CRYPT_NAME="crypt_${LVM_PART_NAME}"
#export STAGE3_URL="https://ftp.fau.de/gentoo/releases/amd64/autobuilds/current-stage3-amd64-openrc-splitusr/stage3-amd64-openrc-splitusr-20240428T163427Z.tar.xz"
export STAGE3_TARBALL="stage3-amd64-openrc-splitusr-20240505T170430Z.tar.xz"
export SWAPSIZE="256M"
export NCPUS="8"
export FETCH_USER="oznt"
export SHORTCUT_RSYNC="y"
export FETCH_PASSWORD="${FETCH_PASSWORD:-s3kr3t}"
export BINPKGS_MIRROR="https://gains.shift-computing.de"
export GLOBAL_USE="X vala lvm"
export VIDEO_CARDS="intel"

prepare_disk
encrypt_partition
create_lvm
format_partitions
mount_partitions "${DISK}"
get_and_unpack_base_system
prepare_chroot
add_bin_repo
add_make_conf
set_lvm_use_flags
prepare_install

assert_file_exists /mnt/gains/etc/resolv.conf "resolv.conf not found"
assert_file_exists /mnt/gains/etc/gentoo-release "gentoo-release not found"
assert_file_exists /mnt/gains/etc/portage/binrepos.conf/gains.conf "gains.conf not found"
assert_file_exists /mnt/gains/etc/portage/make.conf "make.conf not found"
assert_file_exists /mnt/gains/etc/portage/package.use/lvm "lvm use flags not found"
assert_file_exists /mnt/gains/var/db/repos/gentoo/dev-lang/python/metadata.xml "python metadata.xml ot found"

make_conf=$(cat /mnt/gains/etc/portage/make.conf)
binrepos_conf=$(cat /mnt/gains/etc/portage/binrepos.conf/gains.conf)
assert_contain "${make_conf}" vala "vala not found in make.conf"
assert_contain "${make_conf}" j8 "-j8 not found in make.conf"
assert_contain "${binrepos_conf}" "sync-uri = https://gains.shift-computing.de" "sync-uri not found in binrepos.conf"
assert_contain "${binrepos_conf}" "oznt" "oznt not found in binrepos.conf"
assert_contain "${binrepos_conf}" "${FETCH_PASSWORD}" "${FETCH_PASSWORD} not found in binrepos.conf"

video_cards=$(grep VIDEO_CARDS /mnt/gains/etc/portage/make.conf | cut -d"=" -f 2)
echo "video_cards $video_cards"
assert_equal ${video_cards} \"intel\" "intel not found in video_cards"
read -p "Press enter to continue"
echo "All tests passed!"

# vi: sts=4 ts=4 sw=4 ai et
