#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh

function cleanup() {
    # deactivate
    vgchange -a n "${LVM_VG_NAME}"
    vgremove -y "${LVM_VG_NAME}"
    # close the encrypted partition
    cryptsetup close "${CRYPT_NAME}"
}

cleanall() {
    cleanup
    cleanup_lodevice
}

trap cleanall EXIT

setup_lodevice


export DISK="${LDEVICE}"
export LVM_PART_NAME="lvm-test"
export LVM_VG_NAME="${LVM_PART_NAME}vg"
export DISKPASSWORD="verYSekretPassword!"
export CRYPT_NAME="crypt_${LVM_PART_NAME}"
export SWAPSIZE="256M"

prepare_disk
encrypt_partition
create_lvm
format_partitions

assert_contain "$(lsblk -f /dev/loop0p2)" "ext2"
assert_contain "$(blkid /dev/lvm-testvg/root)" "TYPE=\"ext4\""
assert_contain "$(blkid /dev/lvm-testvg/swap)" "TYPE=\"swap\""
assert_contain "$(lsblk -f /dev/loop0p1)" "FAT32"
