#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh

function cleanup() {
    set +e
    umount /mnt/gains/boot/efi
    umount /mnt/gains/boot
    umount /mnt/gains/
    swapoff /dev/lvmtestvg/swap
    # deactivate
    vgchange -a n "${LVM_VG_NAME}"
    vgremove -y "${LVM_VG_NAME}"
    # close the encrypted partition
    cryptsetup close "${CRYPT_NAME}"
}


cleanall() {
    cleanup
    cleanup_lodevice
}

trap cleanall EXIT

setup_lodevice

export DISK="${LDEVICE}"
export LVM_PART_NAME="lvmtest"
export LVM_VG_NAME="${LVM_PART_NAME}vg"
export DISKPASSWORD="verYSekretPassword!"
export CRYPT_NAME="crypt_${LVM_PART_NAME}"
export SWAPSIZE="256M"


prepare_disk
encrypt_partition
create_lvm
format_partitions
mount_partitions "${DISK}"
get_and_unpack_base_system
prepare_chroot


