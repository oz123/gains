#!/bin/bash

set -e

source ../src/gains/functions.sh
source assertions.sh

source ./setup-lodevice-as-disk.sh

function cleanup() {
    # deactivate
    vgchange -a n "${LVM_VG_NAME}"
    vgremove -y "${LVM_VG_NAME}"
    # close the encrypted partition
    cryptsetup close "${CRYPT_NAME}"
}

trap cleanup EXIT

setup_lodevice

export DISK="${LDEVICE}"
export LVM_PART_NAME="lvm-test"
export LVM_VG_NAME="${LVM_PART_NAME}vg"
export DISKPASSWORD="verYSekretPassword!"
export CRYPT_NAME="crypt_${LVM_PART_NAME}"
export SWAPSIZE="256M"

prepare_disk
encrypt_partition

assert_eq "$(calculate_third_partition_name)" "${DISK}p3"

create_lvm

CREATEDE_SWAP_SIZE=$(lvdisplay /dev/lvm-testvg/swap | grep Size | awk '{print $3}')

assert_eq "${CREATEDE_SWAP_SIZE}" "${SWAPSIZE}.00"

read -p "Press enter to continue"
# vi: sts=4 ts=4 sw=4 ai
