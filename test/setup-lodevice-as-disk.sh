#!/bin/bash

set -e

function cleanup_lodevice() {
    losetup -d "${LDEVICE}"
    rm -vf "${DISKPATH}"
    rm -f lodevice.lock
}


DISKSIZE=12G
DISKPATH=/mnt/ramdisk/disk.img

function setup_lodevice() {
    log "Setting up loop device as disk"
    if [ -f lodevice.lock ]; then
        LDEVICE=$(cat lodevice.lock)
    else
        mkdir -pv /mnt/ramdisk
        mount -t tmpfs -o size="${DISKSIZE}" tmpfs /mnt/ramdisk
        chown -R "${USER}":"${USER}" /mnt/ramdisk
        fallocate -l "${DISKSIZE}" "${DISKPATH}"
        LDEVICE=$(losetup -f "${DISKPATH}" --show)
        echo "${LDEVICE}" > lodevice.lock

    fi

    export LDEVICE
}

(return 0 2>/dev/null) && sourced=1 || sourced=0

if [[ $sourced == 1 ]]; then
    echo
    #set +e
    #echo "You can now use any of these functions:"
    #echo ""
    #grep -E ^function "${BASH_SOURCE[0]}" | cut -d" " -f 2 | cut -d"(" -f 1
else
    set -eu
    setup_lodevice "$@"
    trap cleanup_lodevice EXIT
fi
# vi: sts=4 ts=4 sw=4 ai et
