#!/bin/bash

# Copyright (C) 2024 Oz N Tiram <oz.tiram@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

function log() {
    echo "[$(date)] $*" >&2
}

function check_requirements() {
    log "check_requirements: start"

    which parted >/dev/null || (echo "parted is not installed" && exit 1)
    which cryptsetup >/dev/null || (echo "cryptsetup is not installed" && exit 1)
    which pvcreate >/dev/null || (echo "lvm2 is not installed" && exit 1)
    which mkfs.ext2 >/dev/null || (echo "e2fsprogs is not installed" && exit 1)
    which mkfs.ext4 >/dev/null || (echo "e2fsprogs is not installed" && exit 1)
    which mkfs.vfat >/dev/null || (echo "dosfstools is not installed" && exit 1)
    which mkswap >/dev/null || (echo "sys-apps/util-linux is not installed" && exit 1)
    which tar >/dev/null || (echo "tar is not installed" && exit 1)
    which wget >/dev/null || (echo "wget is not installed" && exit 1)
    which rsync >/dev/null || (echo "rsync is not installed" && exit 1)
    which xz >/dev/null || (echo "xz is not installed" && exit 1)
    log "check_requirements: done"
}

function __prepare_disk_efi() {
    parted -a optimal --script "${DISK}" \
        mklabel gpt \
        mkpart primary 1MiB 1024MiB \
        name 1 efi \
        set 1 esp on \
        mkpart primary 1024MiB 2048MiB \
        name 2 boot \
        set 2 boot on \
        mkpart primary 2048MiB 100% \
        set 3 lvm on  \
        name 3 "${LVM_PART_NAME}"
}

function __prepare_disk_bios() {
    parted -a optimal --script "${DISK}" \
        mklabel msdos \
        mkpart primary 1MiB 1024MiB \
        set 1 boot on \
        mkpart primary 1024MiB 100% \
        set 2 lvm on
}

# partition the disk
function prepare_disk() {
    log "prepare_disk: start"
    case "${BOOT}" in
        efi)
            __prepare_disk_efi
            ;;
        bios)
            __prepare_disk_bios
            ;;
        *)
            echo "BOOT must be either efi or bios"
            exit 1
            ;;
    esac
    log "prepare_disk: done"
}

# calculate the name of the third partition
function calculate_third_partition_name() {
    fdisk -l "${DISK}" | tail -n 1 | awk '{print $1}'
}


function encrypt_partition() {
    log "encrypt_partition: start"
    test -z "${DISKPASSWORD}" && echo "DISKPASSWORD is not set" && exit 1
    
    echo -n "DISKPASSWORD:"
    echo -n "${DISKPASSWORD}"

    THIRD_PRATITION="$(calculate_third_partition_name)"
    CRYPT_NAME="crypt_$LVM_PART_NAME"
    cryptsetup -y --batch-mode -s 512 --use-random \
        --key-file <(echo -n "$DISKPASSWORD") \
        --cipher aes-xts-plain64 \
        --pbkdf argon2id \
        luksFormat "${THIRD_PRATITION}"
    cryptsetup open --key-file <(echo -n "${DISKPASSWORD}") "${THIRD_PRATITION}" "${CRYPT_NAME}"

    log "encrypt_partition: done"
}


function open_disk() {
    test -z "${DISKPASSWORD}" && echo "DISKPASSWORD is not set" && exit 1
    THIRD_PRATITION="$(calculate_third_partition_name)"
    CRYPT_NAME="crypt_$LVM_PART_NAME"
    cryptsetup open --key-file <(echo -n "${DISKPASSWORD}") "${THIRD_PRATITION}" "${CRYPT_NAME}"

}
# create lvm partitions
# create swap and root partitions
# note if RAM size is more than 8GB, swap size is 8GB
# else swap size should about 10% of RAM size
function create_lvm() {
    log "create_lvm: start"
    test -z "${LVM_VG_NAME}" && echo "LVM_VG_NAME is not set" && exit 1
    test -z "${SWAPSIZE}" && echo "SWAPSIZE is not set" && exit 1
    test -z "${CRYPT_NAME}" && echo "CRYPT_NAME is not set" && exit 1

    pvcreate /dev/mapper/"${CRYPT_NAME}" >/dev/null 2>&1
    vgcreate "${LVM_VG_NAME}" /dev/mapper/"${CRYPT_NAME}" >/dev/null 2>&1
    lvcreate -L "${SWAPSIZE}" "${LVM_VG_NAME}" -n swap >/dev/null 2>&1
    lvcreate -l 100%FREE "${LVM_VG_NAME}" -n root >/dev/null 2>&1

    log "create_lvm: done"
}

function __check_suffix() {
    local disk
    SUFFIX=$(fdisk -l "${DISK}" | awk '/^\/dev/ {print substr($1, length($1)-1); exit}')
    if [ "$SUFFIX" == "p1" ]; then
        # this is not a mistake, it really is DISK
        disk="${DISK}p"
        echo "${disk}"
    else
        echo "${DISK}"
    fi
}

function __format_partitions_efi() {
    disk=$1

    mkfs.vfat -F32 "${disk}1" 2>&1
    mkfs.ext2 "${disk}2" 2>&1
    mkfs.ext4 /dev/"${LVM_VG_NAME}"/root >/dev/null 2>&1
    mkswap /dev/"${LVM_VG_NAME}"/swap >/dev/null 2>&1
}

function __format_partitions_bios() {
    local disk=$1

    mkfs.ext2 "${disk}1" 
    mkfs.ext4 /dev/"${LVM_VG_NAME}"/root
    mkswap /dev/"${LVM_VG_NAME}"/swap 2>&1
}

# format the partitions
function format_partitions() {
    log "format_partitions: start"
    local disk
    disk=$(__check_suffix)
    echo "disk: ${disk}"
    case "${BOOT}" in
        efi)
            __format_partitions_efi "${disk}"
            ;;
        bios)
            __format_partitions_bios "${disk}"
            ;;
    esac
    log "format_partitions: done"
}

function __mount_partitions_efi() {
    log "mount_partitions_efi: start"
    local device=$1
    local mount_point=gains
    mkdir -p /mnt/"${mount_point}"
    mount /dev/"${LVM_PART_NAME}"vg/root /mnt/"${mount_point}"
    mkdir /mnt/"${mount_point}"/boot
    mount "${device}"2 /mnt/"${mount_point}"/boot
    mkdir /mnt/"${mount_point}"/boot/efi
    mount "${device}"1 /mnt/"${mount_point}"/boot/efi
    swapon /dev/"${LVM_PART_NAME}vg"/swap
    log "mount_partitions_efi: finished"
}

function __mount_partitions_bios() {
    log "mount_partitions_bios: start"
    local device=$1
    local mount_point=gains
    mkdir -p /mnt/"${mount_point}"
    mount /dev/"${LVM_PART_NAME}"vg/root /mnt/"${mount_point}"
    mkdir /mnt/"${mount_point}"/boot
    mount "${device}"1 /mnt/"${mount_point}"/boot
    swapon /dev/"${LVM_PART_NAME}vg"/swap
    log "mount_partitions_bios: finished"
}

function mount_partitions() {
    local disk=$1
    disk=$(__check_suffix) 
    case "${BOOT}" in
        efi)
            __mount_partitions_efi "${disk}"
            ;;
        bios)
            __mount_partitions_bios "${disk}"
            ;;
    esac
    log "mount_partitions: done"
}

function get_and_unpack_base_system() {
    log "get_and_unpack_base_system: start"
    # download stage3
    test -z "${STAGE3_URL}" && test -z "${STAGE3_TARBALL}" && echo "Please set STAGE_3_URL or STAGE_3_TARBAL" && exit 1
    local mount_point=gains
    if [ -z "${STAGE3_TARBALL}" ]; then
        wget "${STAGE3_URL}" -O /mnt/${mount_point}/stage3.tar.xz
    else
        cp "${STAGE3_TARBALL}" /mnt/${mount_point}/stage3.tar.xz
    fi
    # TODO: maybe add a progress bar with pv
    # https://superuser.com/a/1601085
    tar xpf /mnt/${mount_point}/stage3.tar.xz --xattrs-include='*.*' --numeric-owner -C /mnt/${mount_point}
    rm -f /mnt/${mount_point}/stage3.tar.xz

    log "get_and_unpack_base_system: done"
}

function prepare_chroot() {
    local mount_point=gains
    cp -L /etc/resolv.conf /mnt/"${mount_point}"/etc/
    mount --types proc /proc /mnt/"${mount_point}"/proc
    mount --rbind /sys /mnt/"${mount_point}"/sys
    mount --make-rslave /mnt/"${mount_point}"/sys
    mount --rbind /dev /mnt/"${mount_point}"/dev
    mount --make-rslave /mnt/"${mount_point}"/dev

    log "prepare_chroot: done"
}


function add_bin_repo() {
    local mount_point=gains
cat << EOF > gains.conf
[gains]
# repos with higher priorities are preferred when packages with equal
# versions are found in multiple repos
priority = 199
# packages are fetched from here
sync-uri = ${BINPKGS_MIRROR}
fetchcommand = wget --user=${FETCH_USER} --password=${FETCH_PASSWORD} --passive-ftp --tries=3 --timeout=60 -O "\${DISTDIR}/\${FILE}" "\${URI}"

[gentoo]
priority = 99
sync-uri = https://ftp.fau.de/gentoo/releases/amd64/binpackages/23.0/x86-64-v3/

EOF
    mkdir -p /mnt/"${mount_point}"/etc/portage/binrepos.conf/
    mv gains.conf /mnt/"${mount_point}"/etc/portage/binrepos.conf/

    log "add_bin_repo: done"
}

function add_make_conf() {
    log "add_make_conf: start"
    local mount_point=gains
    local mk_conf="/mnt/${mount_point}/etc/portage/make.conf"
    cat << EOF > "${mk_conf}"
# This sets the language of build output to English.
# Please keep this setting intact when reporting bugs.
LC_MESSAGES=C.utf8
COMMON_FLAGS="-march=x86-64-v3 -O2 -pipe"
CFLAGS="\${COMMON_FLAGS}"
CXXFLAGS="\${COMMON_FLAGS}"
FCFLAGS="\${COMMON_FLAGS}"
FFLAGS="\${COMMON_FLAGS}"

MAKEOPTS="-j${NCPUS}"

ACCEPT_LICENSE="* -@EULA"
VIDEO_CARDS="${VIDEO_CARDS}"

FEATURES="userfetch"
PORTAGE_ELOG_SYSTEM="save"

USE="${GLOBAL_USE}"

PORTAGE_ELOG_CLASSES="log warn error qa"
PORTAGE_LOGDIR="/var/log/portage"

FETCHCOMMAND="wget --prefer-family=IPv4 -t 3 -T 60 --passive-ftp -O \"\\\${DISTDIR}/\\\${FILE}\" \"\\\${URI}\""

EMERGE_DEFAULT_OPTS="--autounmask=y"
EOF
    case "${BOOT}" in
        efi)
            echo "GRUB_PLATFORMS=\"efi-64\"" >> "${mk_conf}"
            ;;
        bios)
            echo "GRUB_PLATFORMS=\"pc\"" >> "${mk_conf}"
            ;;
    esac
    log "add_make_conf: done"
}

function set_lvm_use_flags() {
    local mount_point=gains
    cat << EOF > /mnt/"${mount_point}"/etc/portage/package.use/lvm
    sys-fs/lvm2 lvm
EOF
    cat << EOF > /mnt/"${mount_point}"/etc/portage/package.use/sysklogd
    app-admin/sysklogd logrotate
EOF
}

function __prepare_install() {
    if [ -z "${SHORTCUT_RSYNC}" ]; then
        emerge --sync -q
        log "emerge --sync: done"
    fi
    # init gpg keys for portage trusted packages
    getuto
    log "prepare_install: done"
}

function __run_function_in_chroot() {
    local mount_point=gains
    export -f ${1}
    export -f log
    chroot /mnt/"${mount_point}" /bin/bash -c "$1"
}

function enter_chroot(){
    local mount_point=gains
    mkdir -pv /mnt/${mount_point}
    chroot /mnt/"${mount_point}" /bin/bash -c "$1"
}

function prepare_install() {
    local mount_point=gains
    if [ -n "${SHORTCUT_RSYNC}" ]; then
        mkdir -p /mnt/${mount_point}/var/db/repos/gentoo
        rsync -a /var/db/repos/gentoo /mnt/${mount_point}/var/db/repos/ 
    fi
    __run_function_in_chroot __prepare_install
}


function __update_services(){
    emerge --config sys-process/fcron
    rc-update add sshd boot
    rc-update add fcron default
    rc-update add sysklogd boot
}


function update_services() {
    __run_function_in_chroot __update_services
}


function __install_tools() {
    local mount_point=gains
    ln -sv /var/db/repos/gentoo/profiles/default/linux/amd64/23.0/ /etc/portage/make.profile
    echo "net-misc/networkmanager iwd dhcpcd" >> /etc/portage/package.use/networkmanger
    echo ">=sys-kernel/installkernel-28 dracut grub" >> /etc/portage/package.use/installkernel
    echo "sys-firmware/intel-mircocode -dist-kernel -initramfs" >> /etc/portage/package.use/installkernel

    emerge --oneshot sys-apps/portage
    emerge -k -g \
        sys-fs/lvm2 \
        sys-fs/cryptsetup \
        app-admin/logrotate \
        sys-apps/plocate \
        sys-process/fcron \
        app-admin/sysklogd \
        sys-firmware/intel-microcode \
        net-misc/networkmanager \
        app-admin/doas \
        net-misc/openssh \
        sys-kernel/dracut \
        sys-kernel/installkernel
    if [ -n "${INSTALL_FIRMWARE}" ]; then
        emerge -k -g sys-kernel/linux-firmware
    fi
    log "install_tools: done"
}


function install_tools() {
    __run_function_in_chroot __install_tools
}


function set_hostname() {
    local mount_point=gains
    echo "${HOSTNAME}" > /mnt/"${mount_point}"/etc/hostname
}


function __set_timezone() {
    echo "${TIMEZONE}" > /etc/timezone
    emerge --config sys-libs/timezone-data
}


function set_timezone() {
    __run_function_in_chroot __set_timezone
}


function __set_locale() {
    local locals
    IFS=';' read -r -a locals <<< "${LOCALE_LIST}"
    for locale in "${locals[@]}"; do
        echo "${locale}" >> /etc/locale.gen
    done
    unset IFS
    locale-gen
    eselect locale set "${locals[0]% *}"
    env-update
}


function set_locale() {
    __run_function_in_chroot __set_locale
}


function __set_root_password() {
    echo -e "${ROOTPASSWORD}\n${ROOTPASSWORD}" | passwd root
}


function set_root_password() {
    if [ -z "${ROOTPASSWORD}" ]; then
        echo "ROOTPASSWORD is not set"
        read -r -p "Enter root password: " ROOTPASSWORD
    fi
    if [ -z "${ROOTPASSWORD}" ]; then
        echo "ROOTPASSWORD was not given"
        exit 1
    fi
    __run_function_in_chroot __set_root_password
}


function __prepare_bootloader() {
    emerge -k -g sys-boot/grub:2
    case "${BOOT}" in
        efi)
            mount -o remount,rw /sys/firmware/efi/efivars
            grub-install --target=x86_64-efi \
                --efi-directory=/boot/efi --removable
            ;;
        bios)
            grub-install --target=i386-pc "${DISK}"
            ;;
    esac
}

function __set_kernel_cmdline() {
    CMDLINE=$(dracut --print-cmdline)
    CMDLINE="${CMDLINE} rd.luks.allow-discards quiet"
    sed -i "s#\#GRUB_CMDLINE_LINUX=\"\"#GRUB_CMDLINE_LINUX=\"${CMDLINE}\"#g" /etc/default/grub
}

function __configure_grub() {
    grub-mkconfig -o /boot/grub/grub.cfg
}

function prepare_bootloader() {
    log "prepare_bootloader: start"
    __run_function_in_chroot __prepare_bootloader
    __run_function_in_chroot __set_kernel_cmdline
    __run_function_in_chroot __configure_grub
    log "prepare_bootloader: done"
}

function __configure_dracut() {
    mkdir -p /etc/dracut.conf.d
    cat <<EOF > /etc/dracut.conf.d/00gains.conf
hostonly=" no "
omit_dracutmodules+=" nouveau systemd nfs "
dracutmodules+=" bash i18n drm crypt dm lvm nvdimm lunmask resume rootfs-block terminfo udev-rules usrmount base fs-lib shutdown "
add_drivers+=" nvme nvme_core "
early_microcode="yes"
EOF
}

function configure_dracut() {
    __run_function_in_chroot __configure_dracut
}

function __install_kernel() {
        # dracut should be installed automatically the kernel
        echo "sys-kernel/gentoo-kernel-bin initramfs" >> /etc/portage/package.use/gentoo-kernel-bin
        # installkernel script should update grub too
        emerge -k -g sys-kernel/gentoo-kernel-bin
}

function install_kernel() {
    __run_function_in_chroot __install_kernel
    log "install_kernel: done"
}

function __install_xorg() {
    echo "x11-base/xorg-server elogind udev" > /etc/portage/package.use/xorg
    echo "x11-base/xorg-drivers video_cards_dummy video_cards_fbdev video_cards_i915 video_cards_nvidia video_cards_vesa" >> /etc/portage/package.use/xorg

    emerge -k -g x11-base/xorg-server
    if [ "${VIDEO_CARDS}" == "intel" ]; then
        emerge -k -g x11-drivers/xf86-video-{intel,fbdev,vesa}
    fi
}

function __install_mate() {
    echo "x11-misc/lightdm-gtk-greeter branding" > /etc/portage/package.use/mate
    echo "mate-base/mate base bluetooth extras themes" >> /etc/portage/package.use/mate
    echo "media-video/ffmpegthumbnailer gtk gnome" >> /etc/portage/package.use/mate
    echo "gnome-base/gvfs udisks" >> /etc/portage/package.use/mate
    echo "mate-extra/mate-utils udisks" >> /etc/portage/package.use/mate
    echo "mate-extra/mate-system-monitor elogind" >> /etc/portage/package.use/mate
    echo "mate-extra/mate-sensors-applet lm-sensors hddtemp" >> /etc/portage/package.use/mate
    emerge -k -g mate-base/mate lightdm lightdm-gtk-greeter ffmpegthumbnailer
}

function __enable_desktop_services() {
    rc-update add elogind default
    rc-update add NetworkManager default
    rc-update add display-manager default
}

function install_xorg() {
    log "install_xorg start"
    if [[ "${INSTALL_MATE}" == "true" || "${INSTALL_X}" == "true" ]]; then
        __run_function_in_chroot __install_xorg
    fi
    log "install_xorg: done"
}

function install_mate_desktop() {
    log "install_mate_desktop: start"
    if [ "${INSTALL_MATE}" == "true" ]; then
        __run_function_in_chroot __install_mate
        __run_function_in_chroot __enable_desktop_services
    fi
    log "install_mate_desktop: done"
}

function __set_keymaps() {
    sed -i 's/keymap="us"/keymap="'"${CONSOLE_KEYBOARD_LAYOUT}"'"/g' /etc/conf.d/keymaps
}

function __configure_lightdm() {
    envsubst < /tmp/lightdm-gtk-greeter.conf > /etc/lightdm/lightdm-gtk-greeter.conf
    envsubst < /tmp/lightdm.conf > /etc/lightdm/lightdm.conf
    sed -i 's/^\(DISPLAYMANAGER=\).*/\1"lightdm"/' /etc/conf.d/display-manager
    # this is needed for setxkbmap to work
    # as we use it in lightdm.conf
    emerge -g -k x11-apps/setxkbmap
}

function set_keymaps() {
    local mount_point=gains
    log "set_keymap: start"
    __run_function_in_chroot __set_keymaps
    if [ "${INSTALL_MATE}" == "true" ]; then
         cp src/gains/files/lightdm-gtk-greeter.conf /mnt/${mount_point}/tmp/
         cp src/gains/files/lightdm.conf /mnt/${mount_point}/tmp/
         __run_function_in_chroot __configure_lightdm
    fi
    log "set_keymap: end"
}

function set_fstab() {
    local mount_point=gains
    # TODO on real nvme this function broke since DISK2 should be  DISKp2
    # TODO on real nvme this function broke since DISK1 should be  DISKp1
    root_uuid=$(blkid -s UUID -o value /dev/"${LVM_PART_NAME}vg"/root)
    boot_uuid=$(blkid -s UUID -o value "${DISK}"2)
    efi_uuid=$(blkid -s UUID -o value "${DISK}"1)
    swap_uuid=$(blkid -s UUID -o value /dev/"${LVM_PART_NAME}vg"/swap)

    echo "UUID=\"${root_uuid}\" / ext4 defaults 0 1" > /mnt/"${mount_point}"/etc/fstab

    echo "UUID=\"${boot_uuid}\" /boot ext2 defaults 0 2" >> /mnt/"${mount_point}"/etc/fstab
    echo "UUID=\"${efi_uuid}\" /boot/efi vfat defaults 0 2" >> /mnt/"${mount_point}"/etc/fstab
    echo "UUID=\"${swap_uuid}\" none swap sw 0 0" >> /mnt/"${mount_point}"/etc/fstab
    log "set_fstab: done"
}


function cleanup_after_installation() {
    log "cleanup: start"
    local mount_point=gains
    set +e
    pkill -9 dirmngr
    umount  /mnt/"${mount_point}"/dev/shm
    umount  /mnt/"${mount_point}"/dev/mqueue
    umount  /mnt/"${mount_point}"/dev/pts
    umount  /mnt/"${mount_point}"/dev/
    umount  /mnt/"${mount_point}"/sys/kernel/debug
    umount  /mnt/"${mount_point}"/sys/kernel/config
    umount  /mnt/"${mount_point}"/sys/kernel/security
    umount  /mnt/"${mount_point}"/sys/fs/cgroup
    umount  /mnt/"${mount_point}"/sys/fs/pstore
    umount  /mnt/"${mount_point}"/sys
    umount  /mnt/"${mount_point}"/boot/efi
    umount  /mnt/"${mount_point}"/boot
    umount  /mnt/"${mount_point}"/proc
    swapoff /dev/"${LVM_PART_NAME}vg"/swap
    umount  /dev/mapper/"${LVM_PART_NAME}vg"-root
    # deactivate the volume group
    vgchange -a n "${LVM_VG_NAME}"
    #vgremove -y "${LVM_VG_NAME}"
    cryptsetup close "${CRYPT_NAME}"
    test -d /mnt/"${mount_point}"/ && rmdir -v /mnt/${mount_point}
    log "cleanup: done"
}

# vi: sts=4 ts=4 sw=4 ai et
