Gentoo Automatic Intaller Script
================================

An automated installer for gentoo with encrypted disk and LVM.

Goals
-----

Provide a simple installer for installing gentoo on a VM or Physical machine.
The installer creates:

1. A bootable gentoo
2. Using grub2
3. dracut as initrd manager
4. Gentoo binary kernel
5. FDE + LVM
6. SSH server and a few other services
7. A root user.

That's enough to get you started with the rest.

Also, there should be a test suite allowing to develop
new features more easily.

Non Goals
---------
1. Supporting any other initrd (e.g. genkernel)
2. Supporting any other bootloader
3. Supporting any other disk layout

These non goals might change overtime ...

Usage
-----

The script is relativly easy to understand (I hope so).
You should edit the config section at the top of gains.sh (it's just bash variables)
and run the script as ROOT:

```
# bash ./gains.sh
```

Why not use X?
--------------

There are many automated scripts\installers out there.
I've used `getch` and it's great! But it's written in ruby, which
means it's hard to study the code.
I've used `oddlama/gentoo-install` and it's great too! It has a lot
installation layouts and it's not written in ruby, which makes it
easier to understand. But ... it's still complicated, because it has
so many options. However, it does not support LVM and encrypted swap.
Both have been a great insipiration, and without them this installer
would not have been written.

Credits
-------

1. https://amedeos.github.io/gentoo/2020/07/13/install-gentoo-with-uefi-luks-lvm-and-systemd.html  
2. https://www.nkly.me/blog/2019/install-gentoo/  
3. https://github.com/szorfein/getch
4. https://github.com/oddlama/gentoo-install
