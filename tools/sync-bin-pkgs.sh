#!/bin/bash

# This script syncs the binpkgs from the host to the nginx-gains pod
# It uses kubectl rsync to copy the files from the host to the pod
# It also sets up a tunnel to the build hosts to allow port forwarding
# of the kubernetes api to the build host.
# It assumes that your ssh config has:

#Host gains-host
#   HostName YorIP
#   LocalForward 16443  127.0.0.1:16443     # microk8s
#   User youruser

# The gains pod has nginx for serving the package and a portage container
# from gentoo/portage that has the portage tree mounted at /var/db/repos/gentoo

set -e

ssh -F /etc/portage/post-sync-ssh-conf -N ${BINPKGSHOST?} &

SSH_PID=$!

# Wait for the tunnel to be up using nc
until nc -zv localhost 16443; do
   echo "Waiting for tunnel to be up..."
   sleep 0.5
done
echo "Tunnel is up."

export CLIENTCONF="--kubeconfig=${KUBECONFIG} --context=${CONTEXT} -n gains"

POD=$(kubectl ${CLIENTCONF} get pods -l app=nginx-gains -o jsonpath='{.items[0].metadata.name}')
kubectl rsync ${CLIENTCONF} -c portage -- -av /srv/portage/binpkgs/ "${POD}":/var/cache/binpkgs/
kubectl ${CLIENTCONF} exec ${POD} -c portage -- /bin/bash -c "eclean packages -u && emaint binhost --fix"

kill -9 ${SSH_PID}
