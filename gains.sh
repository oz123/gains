#!/bin/bash

# Copyright (C) 2024 Oz N Tiram <oz.tiram@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e 

# CONFIGURATION

# edit these to match your system
export DISK="/dev/nvme0n1"
export LVM_PART_NAME="gentoo"
export LVM_VG_NAME="${LVM_PART_NAME}vg"
export SWAPSIZE="8G"
export # if this variable is set, the installer will not download the recent tarball
export STAGE3_TARBALL=""
export STAGE3_URL="https://ftp.fau.de/gentoo/releases/amd64/autobuilds/current-stage3-amd64-openrc-splitusr/stage3-amd64-openrc-splitusr-20240428T163427Z.tar.xz"
export BINPKGS_MIRROR="https://ftp-stud.hs-esslingen.de/pub/Mirrors/gentoo/releases/amd64/binpackages/23.0/x86-64-v3/"
export ROOTPASSWORD="password"
export TIMEZONE="Europe/Berlin"
export BINPKGS_MIRROR="https://gains.shift-computing.de"
export GLOBAL_USE="X acl alsa bash-completion bluetooth elogind cups dbus icu introspection policykit jpeg pulseaudio xinerama sqlite tk vim-syntax udev -qt5 vala"
export # https://wiki.gentoo.org/wiki//etc/portage/make.conf#VIDEO_CARDS
export VIDEO_CARDS="intel"
export FETCH_USER="gains"
export FETCH_PASSWORD="snaig"
export HOSTNAME="gentoo"
export # The list of locales to be generated, separated by a semicolon, e.g. "en_US.UTF-8 UTF-8;de_DE.UTF-8 UTF-8"
export # The first locale in the list will be set as the system default
export # The format is "NAME CHARSET"
export LOCALE_LIST="en_US.UTF-8 UTF-8;de_DE.UTF-8 UTF-8"
export TIMEZONE="Europe/Berlin"
export INSTALL_FIRMWARE="true"
export BOOT="efi" # efi or bios
export X11_KEYBOARD_LAYOUT="us" # de-latin1
export CONSOLE_KEYBOARD_LAYOUT="de-latin1"
export INSTALL_X="true"
export INSTALL_MATE="true"

# do not edit below this line
source src/gains/functions.sh



function main() {
    check_requirements
    prepare_disk
    encrypt_partition
    create_lvm
    format_partitions
    mount_partitions "${DISK}"
    get_and_unpack_base_system
    prepare_chroot
    add_bin_repo
    add_make_conf
    set_lvm_use_flags
    prepare_install
    install_tools
    update_services
    set_hostname
    set_timezone
    set_locale
    set_root_password
    prepare_bootloader
    configure_dracut
    install_xorg
    install_mate_desktop
    set_keymaps
    install_kernel
    set_fstab
    cleanup_after_installation
}

if [ x"${1}" == x"--shell" ]; then
    open_disk
    mount_partitions
    prepare_chroot
    enter_chroot
else
    main
fi

# vi: sts=4 ts=4 sw=4 ai et
